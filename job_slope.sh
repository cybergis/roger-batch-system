#!/bin/bash
### Example job submission on Roger (roger-login.ncsa.illinois.edu)

### Set the job name
#PBS -N pit_slope_xxxxxx

### Use the bourne shell
#PBS -S /bin/bash

### To send email when the job is completed:
### be --- before execution
### ae --- after execution
#PBS -m ae
#PBS -M xxxx@xxxx.edu

### Specify the number of cpus for your job. 
#PBS -l nodes=2:ppn=20

### Tell PBS the anticipated run-time for your job, where walltime=HH:MM:SS
#PBS -l walltime=1:00:00

dem_path="/projects/EOT/fall16/cybergis/dem_input"
output_path="/projects/class/jhub/users/"$USER"/taudem"

module load mpich taudem

echo "Calculating pit removed DEM"
mpirun -n $PBS_NP pitremove -z $dem_path -fel $output_path/pitrem

sleep 1
echo "Calculating slope and aspect"
mkdir -p $output_path/slope
mkdir -p $output_path/aspect
mpirun -n $PBS_NP d8flowdir -fel $output_path/pitrem -sd8 $output_path/slope -p $output_path/aspect

# End of PBS script
