#!/bin/bash
### Example job submission on Roger (roger-login.ncsa.illinois.edu)

### Set the job name
#PBS -N pitrmv_xxxxxx

### Use the bourne shell
#PBS -S /bin/bash

### To send email when the job is completed:
### be --- before execution
### ae --- after execution
#PBS -m ae
#PBS -M xxxx@xxxx.edu

### Specify the number of cpus for your job. 
#PBS -l nodes=2:ppn=20

### Tell PBS the anticipated run-time for your job, where walltime=HH:MM:SS
#PBS -l walltime=1:00:00

module load mpich taudem
echo "Calculating pit removed DEM"
mkdir -p /projects/class/jhub/users/$USER/taudem/pitrem
mpirun -n $PBS_NP pitremove -z /projects/EOT/fall16/cybergis/dem_input -fel /projects/class/jhub/users/$USER/taudem/pitrem

# End of PBS script
