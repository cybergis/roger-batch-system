# README #

This repository contains example scripts for ROGER HPC batch system training.

## First, edit and run job\_pitrmv.sh  
* Edit the job name and the email  
* Notice that your username is being provided by the system from an environment variable: $USER  
* Save your edits, then run it with `qsub job_pitrmv.sh`  
* Check on the job progress with `qstat`

## Investigate the output
* When the job is done, you can read the log files in the directory.
    * They end with e+JOBNUMBER for errors, and o+JOBNUMBER for standard output.
* Check with size of the output file with `ls -lh`  
* See more of the output metadata with gdal:  
    `module load gdal2-stack`  
    `gdalinfo /path/to/raster.tif`  

## Next, edit and run job\_slope.sh  
* Edit the job name and the email  
* Notice that now the whole base of the output path is an environment variable  
* Save your edits, then run it with `qsub job_slope.sh`  

## Finally, edit and run job\_csar.sh  
* This runs four TauDEM operations, and matches the exercise in the CyberGIS Gateway tutorial.  
* Edit the job name and the email  
* Save your edits, then run it with `qsub job_slope.sh`