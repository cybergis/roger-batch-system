#!/bin/bash
### Replicates workflow in Gateway example

### Set the job name
#PBS -N csar_xxxxxx

### Use the bourne shell
#PBS -S /bin/bash

### To send email when the job is completed:
### be --- before execution
### ae --- after execution
#PBS -m ae
#PBS -M xxxx@xxxx.edu

### Specify the number of cpus for your job. 
#PBS -l nodes=2:ppn=20

### Tell PBS the anticipated run-time for your job, where walltime=HH:MM:SS
#PBS -l walltime=1:00:00

dem_path="/projects/EOT/fall16/cybergis/dem_input"
output_path="/projects/class/jhub/users/"$USER"/taudem"

module load mpich taudem

echo "Calculating pit removed DEM"
# pitrem folder already exists if you've run job_pitrmv.sh
mpirun -n $PBS_NP pitremove -z $dem_path -fel $output_path/pitrem

sleep 1
echo "Calculating slope and aspect"
# slope and aspect folders already exist if you're run job_pitrmv_slope.sh
mpirun -n $PBS_NP d8flowdir -fel $output_path/pitrem -sd8 $output_path/slope -p $output_path/aspect

echo "Calculating D8 Accumulation"
mkdir $output_path/aread8
mpirun -n $PBS_NP aread8 -p $output_path/aspect -ad8 $output_path/aread8
sleep 5

# Slope area ratio (sar) is similar to topographic wetness index, which is not available in TauDEM 5.2
echo "Calculating Slope Area Ratio"
mkdir $output_path/sar
mpirun -n $PBS_NP slopearearatio -slp $output_path/slope -sca $output_path/aread8 -sar $output_path/sar

# End of PBS script
